INTRODUCTION
------------

This module is designed to help translators decide which interface strings most
urgently require translation.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/interface_string_stats

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/interface_string_stats

REQUIREMENTS
------------

This module requires the core Locale module.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

   - Administer Interface string stats configuration

     Grants access to the configuration page for the module.

 * Change the module configuration in Administration » Configuration »
   Regional and language » Interface String Statistics configuration.

 * View the translation string usage statistics at Administration »
   Configuration » Regional and language » User interface translation. These are
   ordered by the most viewed.

 * To enable the capturing of statistics, select 'Yes' under 'Capture statistics
   on interface strings'.

 * Select which roles will NOT be counted when capturing statistics under
   'Roles to filter'.
